import React, { useState } from 'react';
import TestPhoto from '../static/test.png';
import { Add, Remove } from '@material-ui/icons';

export const CanvasComponent = () => {
  const [isActiveFirstButton, setIsActiveFirstButton] = useState(false);
  const [isActiveSecondButton, setIsActiveSecondButton] = useState(false);
  const [isActiveThirdButton, setIsActiveThirdButton] = useState(false);

  return (
    <div className="position-relative">
      <div className="icons-list justify-between">
        <button className={'icon-button'} onClick={() => setIsActiveFirstButton(!isActiveFirstButton)}>
          <span className={`${isActiveFirstButton && 'active-button'} icons-margin`}><i class="flaticon-move-1"></i></span>
        </button>
        <button className={'icon-button'} onClick={() => setIsActiveSecondButton(!isActiveSecondButton)}>
          <span className={`${isActiveSecondButton && 'active-button'} icons-margin`}><i class="flaticon-diagonal-arrow"></i></span>
        </button>
        <button className={'icon-button'} onClick={() => setIsActiveThirdButton(!isActiveThirdButton)}>
          <span className={`${isActiveThirdButton && 'active-button'} icons-margin`}><i class="flaticon-square"></i></span>
        </button>
      </div>
      <img src={TestPhoto} alt="test" className="canvas-img"></img>
      <div className="canvas-buttons">
        <button className="icon-button control-button"><Add /></button>
        <button className="icon-button control-button"><Remove /></button>
      </div>
    </div>
  )
}