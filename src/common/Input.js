import React from 'react';

export const Input = ({
  title,
  placeholder
}) => {
  return (
    <div className="justify-between margin-input">
      <div>{title}</div>
      <input
        type="text"
        placeholder={placeholder}
      />
    </div>
  )
}