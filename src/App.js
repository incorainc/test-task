import React from 'react';
import { ChevronLeft } from '@material-ui/icons';
import { CanvasComponent } from './common/CanvasComponent';
import { Header } from './common/Header';
import { SelectComponent } from './common/Select';
import { Input } from './common/Input';

function App() {
  const options = [
    { id: 1, name: "First" },
    { id: 2, name: "Second" },
    { id: 3, name: "Third" },
  ];
  return (
    <>
      <Header />
      <div className="container">
        <div className="justify-between header-tab">
          <button className="justify-around back-btn"><ChevronLeft />Back</button>
          <SelectComponent options={options} title="Store Analytics" />
        </div>
        <div className="input-block">
          <Input title="Till Name" placeholder="Type in..." />
          <div className="justify-between margin-input">
            <div>Department</div>
            <SelectComponent options={options} title="Choose" className="custom-select-container-input" />
          </div>
          <div className="justify-between margin-input">
            <div>Camera</div>
            <SelectComponent options={options} title="Choose" className="custom-select-container-input" />
          </div>
        </div>
        <p className="text-style">Mark the area in of the till in the floorplan below:</p>
        <CanvasComponent />
        <div className="save-button-container">
          <button className="save-button">Save</button>
        </div>
      </div>
    </>
  );
}

export default App;
