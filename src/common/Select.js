import React, { useState, useEffect } from 'react';

export const SelectComponent = ({
  options,
  className,
  title
}) => {
  const [defaultText, setDefaultText] = useState(title);
  const [showOptionList, setShowOptionList] = useState(false);

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    }
  }, [])

  const handleClickOutside = e => {
    if (
      !e.target.classList.contains("custom-select-option") &&
      !e.target.classList.contains("selected-text")
    ) {
      setShowOptionList(false);
    }
  };

  const handleListDisplay = () => {
    setShowOptionList(!showOptionList)
  };

  const handleOptionClick = e => {
    setShowOptionList(false);
    setDefaultText(e.target.getAttribute("data-name"));
  };
  return (
    <div className={className ? "custom-select-container custom-select-container-input" : "custom-select-container"}>
        <div
          className={showOptionList ? "selected-text active" : "selected-text"}
          onClick={handleListDisplay}
        >
          {defaultText}
        </div>
        {showOptionList && (
          <ul className="select-options">
            {options.map(option => {
              return (
                <li
                  className="custom-select-option"
                  data-name={option.name}
                  key={option.id}
                  onClick={handleOptionClick}
                >
                  {option.name}
                </li>
              );
            })}
          </ul>
        )}
      </div>
  )
}