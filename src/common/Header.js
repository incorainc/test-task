import React from 'react';
import HeaderLogo from '../static/header.png';

export const Header = () => {
  return (
    <header className="container-header">
      <div className="header-title">Sloane sq. - Till page</div>
      <img src={HeaderLogo} alt="test" className="logo" />
    </header>
  )
}